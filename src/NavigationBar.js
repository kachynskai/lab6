import logo from "./image/logo.png"
import "./navigationBar.css"
function NavigationBar(){
    return(
    <header className="head">
        <img src={logo} alt="logo"/>
        <nav className="nav">
            <ul>
                <li><button>Home</button></li>
                <li><button>Map</button></li>
                <li><button>Languages</button></li>
                <li><button>API</button></li>
            </ul>
        </nav>

    </header>
)
}export default NavigationBar