import React from "react";
import LoginForm from "./LoginForm";
import {Route, Routes} from "react-router";
import DashBoard from "./DashBoard";
function App() {
  return (
    <div className="App">
        <Routes>
        <Route path="/dashboard" element={<DashBoard/>}/>
        <Route path="/" element={<LoginForm/>}/>
        </Routes>

    </div>
  );
}

export default App;
