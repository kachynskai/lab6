import React, {useState} from "react";
import './loginForm.css'
import { useNavigate} from 'react-router-dom';
import Cookies from "js-cookie"
function LoginForm(){
    const [login, setLogin]= useState("");
    const [password,setPassword]=useState("");
    const [remember, checkRemember]=useState(false);
    const [errorLogin, setErrorLogin]=useState("");
    const [errorPass, setErrorPass]=useState("");
    const myLogin="someone@gmail.com"
    const myPass="iryna2"
    const navigation=  useNavigate();
   const validate=()=>{
       if(!login.includes('@')&&login!==""){
           setErrorLogin("Введіть валідну пошту. Вона повинна містити '@'!")
           setLogin("");
       }
       if(myLogin===login&&myPass===password){
           Cookies.set("loggedIn", "true");
           navigation("/dashboard")
       }else if(myPass!==password){
           setErrorPass("Ви ввели неправильний пароль! Будь ласка, спробуйте знову");
           setPassword("");
       }if(myLogin!==login){
           setErrorLogin("Ви ввели неправильний e-mail! Будь ласка, спробуйте знову")
           setLogin("");
       }
    }
        return(
            <div className="form-container">

                <form>
                    <h2>Log to Web App</h2>
                    <hr/>
                    <div className="form-element">
                        <label htmlFor="login">E-mail :</label>
                        <input placeholder="Input login"
                               type="text"
                               id="login"
                               value={login}
                               onChange={(e)=>{setLogin(e.target.value); setErrorLogin("")}}
                        />
                        <div className="error">{errorLogin}</div>
                    </div>
                    <hr/>
                    <div className="form-element">
                        <label htmlFor="password">Password :</label>
                        <input placeholder="Input password"
                               type="password"
                               id="password"
                               value={password}
                               onChange={(e)=>{setPassword(e.target.value); setErrorPass("")}}
                        />
                    </div>
                    <div className="error">{errorPass}</div>
                    <hr/>

                    <input type='checkbox'
                           id="remember"
                           checked={remember}
                           onChange={()=>{checkRemember(!remember)}}
                    />
                    <label htmlFor="remember">Remember me</label>
                    <div>
                    <button type="button" onClick={validate}>Login</button>

                    </div>
                </form>
            </div>
        )

}export default LoginForm;