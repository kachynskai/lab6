
import { useEffect, useState } from "react";
import "./dashBoard.css";
import WeatherItem from "./WeatherItem";
import NavigationBar from "./NavigationBar";
import { BeatLoader } from "react-spinners";
import Cookies from "js-cookie"
import {useNavigate} from "react-router-dom";

function DashBoard() {
    const [weather, setWeather] = useState(null);
    const [isLoading, setLoading] = useState(true);
    const navigation=  useNavigate();
    useEffect(() => {
        const loggedIn=Cookies.get("loggedIn");
        if(!loggedIn){
            navigation("/");
            alert("Спочатку потрібно зарєєструватись!!");
            return;
        }
        Cookies.remove("loggedIn");
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const { latitude, longitude } = position.coords;
                fetch(`https://api.weatherapi.com/v1/forecast.json?key=f99baae0ef1a4d1187a94526231511&q=${latitude},${longitude}&days=5&aqi=no&alerts=yes`)
                    .then((info) => info.json())
                    .then((data) => {
                        setWeather(data);
                        setLoading(false);
                    })
                    .catch((error) => {
                        console.error("Exception: ", error);
                        setLoading(false);
                    });
            },
            (error) => {
                console.error("Error getting location:", error);
                setLoading(false);
            }
        );
    }, [navigation]);

    if (!isLoading && weather) {
        return (

            <div>
                <NavigationBar/>
                <div className="top">
                    <div className="left-info"><h1>{weather.location.name}</h1>, {weather.location.country}</div>
                    <div className="right-info">
                        <div><h4>Time: </h4> {new Date(weather.current.last_updated).toLocaleTimeString()}</div>
                        <div><h4>Sunrise:</h4>  {weather.forecast.forecastday[0].astro.sunrise}</div>
                        <div><h4>Sunset: </h4> {weather.forecast.forecastday[0].astro.sunset}</div>
                    </div>
                </div>
                <div className="main-part">
                    {weather.forecast.forecastday.map((day) => (
                        <WeatherItem key={day.date} item={day} />
                    ))}
                </div>
            </div>
        );
    }
    return (
        <div className="loading-spinner">
            <BeatLoader color={"#123abc"} loading={true} size="30px"/>
            <p>Зачекайте хвильку, триває завантаження</p>
        </div>
    )
}

export default DashBoard;
