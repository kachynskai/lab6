import React from "react";
import "./weatherItem.css"
function WeatherItem(props){
    const currentDate = new Date();
    const tomorrowDate = new Date(currentDate);
    tomorrowDate.setDate(currentDate.getDate() + 1);
    const itemDate=new Date(props.item.date);
    let displayDate;
    if(currentDate.toDateString()===itemDate.toDateString()){
        displayDate="Today";
    }else if(tomorrowDate.toDateString()===itemDate.toDateString()){
        displayDate="Tomorrow";
    }else{
        displayDate=new Date(props.item.date).toLocaleDateString("en-US", { weekday: "short",  day: "numeric", month: "short" });
    }
    return (
        <div className="weather-item" key={props.date}>
            <h3 className="headers">{displayDate}</h3>
            <div className="icons">
                <img src={props.item.day.condition.icon} alt="weather logo"/>
                <p>{props.item.day.condition.text}</p>
            </div>
            <div>Max : {props.item.day.maxtemp_c}°C</div>
            <div>Min : {props.item.day.mintemp_c}°C</div>
            <div><h4> Wind speed:</h4> {props.item.day.maxwind_mph} mph</div>
            <div><h4>Humidity : </h4>{props.item.day.avghumidity}%</div>
            <div><h4>Visibility : </h4>{props.item.day.avgvis_miles} miles</div>
            <div> <h4>Pressure: </h4>{props.item.hour[11].pressure_mb} mb</div>
            <div><h4>Confidence:</h4> {props.item.day.daily_chance_of_rain}%</div>

        </div>
    )
}
export default WeatherItem